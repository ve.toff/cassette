# *Cassette Project* 
#### *Cassette* - простое интернет-радио со спокойной музыкой (Lofi, Jazz, Classic).

> В дальнейшем планируется перенести фронтенд на [Vue.js](https://vuejs.org/)

## В разработке применяется:

-  [Python 3.8.5+](https://www.python.org/downloads/release/python-385/)
-  [Django 3](https://www.djangoproject.com/)
-  [Icecast](https://icecast.org/)
-  [Liquidsoap](https://www.liquidsoap.info/)
-  [Howler.js](https://howlerjs.com/)

## Установка
### Создание виртуального окружения
```
python -m venv venv
```
### Активация виртуального окружения
###### [Windows]
```
venv\Scripts\activate.bat
```
###### [Linux]
```
source venv/bin/activate
```
### Установка пакетов
```
pip install -r requirements.txt
```
### Подготовка базы данных
```
python manage.py makemigrations
```
```
python manage.py migrate
```
### Запуск сервера
```
python manage.py runserver
```
## Запуск в Docker (локально)

### Указать IP-адрес хоста в liquidsoap/.env
```
ICECAST_HOST=<Ваш адрес>
```
### Запуск docker-compose
```
docker-compose -f docker-compose-dev.yml up --build
```

## В разработке принимали участие

#### &#128007; Зарема [*https://gitlab.com/rozzzmarin*](https://gitlab.com/rozzzmarin)
#### &#127938; Константин [*https://gitlab.com/kostsr*](https://gitlab.com/kostsr)  
> Все права на музыку, которая используется в проекте в демонстрационных целях, принадлежат их соответствующим владельцам.